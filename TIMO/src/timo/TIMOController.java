package timo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebView;
import javafx.util.StringConverter;

public class TIMOController implements Initializable {
	

	
    @FXML
    private AnchorPane anchorPane;
	
    @FXML
    private TabPane mainTabPane;
	
    @FXML
    private Tab newPackageTab;
    
    @FXML
    private Tab logTab;
    
    @FXML
    private Tab smartPostTab;
    
    @FXML
    private WebView web;

    @FXML
    private ComboBox<String> cityCombo;

    @FXML
    private ComboBox<Package> packageCombo;

    @FXML
    private Button add2MapButton;

    @FXML
    private Button sendButton;

    @FXML
    private Button clearMapButton;
    
    @FXML
    private Button createNewPackageButton;

    @FXML
    private Button cancelNewPackageButton;
    
    @FXML
    private ComboBox<String> fromCityComboBox;

    @FXML
    private ComboBox<String> toCityComboBox;

    @FXML
    private ComboBox<Smartpost> fromSmartComboBox;

    @FXML
    private ComboBox<Smartpost> toSmartComboBox;

    @FXML
    private ComboBox<Item> itemComboBox;
    
    @FXML
    private TextField itemNameField;

    @FXML
    private TextField itemWeightField;

    @FXML
    private TextField heightField;

    @FXML
    private TextField widthField;

    @FXML
    private TextField depthField;

    @FXML
    private CheckBox fragileCheckBox;

    @FXML
    private RadioButton thrdClassSelection;

    @FXML
    private RadioButton firstClassSelection;

    @FXML
    private RadioButton scndClassSelection;
    
    @FXML
    private ListView<String> packageView;

    @FXML
    private TextField sumOfPackages;
    
    @FXML
    private Label errorLabel;
    
    @FXML
    private Button pakckageHistory;
    
    @FXML
    private Button getLogDateButton;
    
    @FXML
    private Label classInfoLabel;

    @FXML
    private DatePicker logDatePicker;

	private ArrayList<String> p;
    
    
    @Override
	public void initialize(URL location, ResourceBundle resources) {
    	
    		
    		//Storage singleton
    		Storage s1 = null;
    		s1 = Storage.getInstance();
    		
    		
    		//newPackageTab    	
		ToggleGroup classButtonGroup = new ToggleGroup();
		firstClassSelection.setToggleGroup(classButtonGroup);
		firstClassSelection.setSelected(true);
		scndClassSelection.setToggleGroup(classButtonGroup);
		thrdClassSelection.setToggleGroup(classButtonGroup);
		firstClassInfoAction(null);
		//System.out.println(classButtonGroup.getSelectedToggle().toString().contains("1."));
	
		
		itemComboBox.getItems().add(new Item(8000, 25, "Levypaino", false));		
		itemComboBox.getItems().add(new Item(9000, 1, "Isoäidin posliinivati", true));
		itemComboBox.getItems().add(new Item(100, 0.1, "Korttipakka", false));		
		itemComboBox.getItems().add(new Item(6000000, 50, "Pingispöytä", false));
		
		
		//SmartPostTab
		smartpostHandler sph = null;	
		sph = smartpostHandler.getInstance();
		
		//Creating Obsevable list for comboboxes
		ObservableList<String> oList = FXCollections.observableArrayList(sph.getCity_array());
		
		cityCombo.setItems(oList);
		fromCityComboBox.setItems(oList);
		toCityComboBox.setItems(oList);

		//Loading the map
		web.getEngine().load(this.getClass().getResource("index.html").toExternalForm());;
		 
		//Log calender formatting
		
		String dateFormat = "dd.MM.yyyy";		
		logDatePicker.setConverter(new StringConverter<LocalDate>() {
		     DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(dateFormat);		     
		     
		     @Override 
		     public String toString(LocalDate date) {
		         if (date != null) {
		             return dateFormatter.format(date);
		         } else {
		             return null;
		         }
		     }

		     @Override 
		     public LocalDate fromString(String string) {
		         if (string != null && !string.isEmpty()) {
		             return LocalDate.parse(string, dateFormatter);
		         } else {
		             return null;
		         }
		     }
		 });
		
		
	}
    
    @FXML
    public void add2MapAction(ActionEvent event) {
    	ArrayList<Smartpost> spByCity_array;
    smartpostHandler.getInstance();
    spByCity_array=smartpostHandler.getInstance().getSmartpostListByCity(cityCombo.valueProperty().getValue());   
    	String code, city, address, availability, script;
    	    	
    	
   	for(int i=0; i<spByCity_array.size(); i++) { //All Smartpost from chosen city
   		
    		code=spByCity_array.get(i).getCode();
    		city=spByCity_array.get(i).getCity();
    		address=spByCity_array.get(i).getAddress();
    		availability=spByCity_array.get(i).getAvailability();
    		script="document.goToLocation('"+address+", "+code+ " " +city+"', '"+availability+"', 'orange')";
    		web.getEngine().executeScript(script); //Drawing Smartpostmachines on the map
    		
   	}
    }

    @FXML
    void clearMapAction(ActionEvent event) {
    	web.getEngine().executeScript("document.deletePaths()");
    	
    }

    @FXML
    void sendAction(ActionEvent event) {
    	String script;
    	int category;
  //  	int i=0;
    	String line;
    ArrayList<String> p;
    String broken = "";
    	
    if(!packageCombo.getSelectionModel().isEmpty()) {
    	p=packageCombo.getValue().fromToPoints; //ArrayList, which contains coordinates
    	category=packageCombo.getValue().category;
    	
    script="document.createPath("+ p +", 'blue', "+category+")";
    
    	web.getEngine().executeScript(script); //Creating path
    	
   
    
    
    	//Adding to log
    	String timeStamp = new SimpleDateFormat("dd.MM.yyyy").format(Calendar.getInstance().getTime());
    	
    	boolean b = packageCombo.getItems().get(packageCombo.getSelectionModel().getSelectedIndex()).isBroken;
   
    	if(b == true) {
    		broken = "Tuote valitettavasti rikkoontui";
    	} else {
    		broken = "Tuote säilyi ehjänä";
    	}
    	
    line= timeStamp+" "
    			+packageCombo.getItems().get(packageCombo.getSelectionModel().getSelectedIndex()).item.name +", "
		    	+packageCombo.getItems().get(packageCombo.getSelectionModel().getSelectedIndex()).fromSmartpost.getCity() + " "
		    	+packageCombo.getItems().get(packageCombo.getSelectionModel().getSelectedIndex()).fromSmartpost.getPostoffice()+ " -> "
		    	+packageCombo.getItems().get(packageCombo.getSelectionModel().getSelectedIndex()).toSmartpost.getCity() + " "
		    	+packageCombo.getItems().get(packageCombo.getSelectionModel().getSelectedIndex()).toSmartpost.getPostoffice() + ", Matka: "
		    +packageCombo.getItems().get(packageCombo.getSelectionModel().getSelectedIndex()).traveldist + "km, "
		    + broken;
		     	
    //Deletes the package from the list
    packageCombo.getItems().remove(packageCombo.getSelectionModel().getSelectedIndex());
    try
    {
    		FileWriter fw = new FileWriter("Loki.txt",true); //the true will append the new data
        fw.write(line + "\n"); //Appends the package information to the file
        fw.close();
    }
    	catch(IOException e)
    {
        System.err.println("IOException: " + e.getMessage());
    }
    
    
    packageView.getItems().add(line);
    sumOfPackages.setText(Integer.toString(packageView.getItems().size()));

    
    }
    
    }

    @FXML
    void createNewPackageAction(ActionEvent event) {
    	
    	int v = 0;
    	double w = 0;
    	String n = "";
    	p = new ArrayList<String>();
    	Boolean f = null;
    	Item i = null;
    	String script;
    	Object d = null; //The distance from the script
    	int d1 = 0; //The distance converted to int
    	Package fc = null;
    	
    	
    	errorLabel.setText("");
    	
    		if(itemComboBox.getSelectionModel().getSelectedIndex() != -1) {
    			i = itemComboBox.getSelectionModel().getSelectedItem();
    			v = i.getVolume();
    			w = i.getWeight();
    	
    			
    			
    			
    			
    		} else {
    			try {
    			v = (int)Double.parseDouble(widthField.getText())*
    					(int)Double.parseDouble(heightField.getText())*
    					(int)Double.parseDouble(depthField.getText());
    					//calculates the volume of the item
        		n = itemNameField.getText();
        		w = (int)Double.parseDouble(itemWeightField.getText());
        		f = fragileCheckBox.isSelected();
        		
        		i = new Item(v, w, n, f);
        		
    			} catch (NumberFormatException ex) {
    				errorLabel.setText("Tarkista syöttämäsi tiedot");
    				return;
    			}
    		}
    		
    	try {
    			//Creating ArrayList for the createPath-script
    			p.clear();
	    		p.add(fromSmartComboBox.getSelectionModel().getSelectedItem().getLat().toString());
	    		p.add(fromSmartComboBox.getSelectionModel().getSelectedItem().getLng().toString());
	    		p.add(toSmartComboBox.getSelectionModel().getSelectedItem().getLat().toString());
	    		p.add(toSmartComboBox.getSelectionModel().getSelectedItem().getLng().toString());
	    		
	    		
	    		//Taking only the distance between locations for determing the package category
	        script="document.createPath("+ p +", 'transparent', "+ 1 +")";
	        d = web.getEngine().executeScript(script);
	     	
	     	d1 = (int)Double.parseDouble(d.toString());
	    		
	       // System.out.println(d);
	        
	    		
    		} catch (NullPointerException ex) {
    			errorLabel.setText("Valitse haluamasi automaatit ensin");
    			return;
		
	}
		

	   
    		
    		//fc = new FirstClass(v, w, d1 , i, p);
    		
    		Smartpost from = fromSmartComboBox.getValue();
    		Smartpost to = toSmartComboBox.getValue();
    		
    		String pc = Storage.getInstance().selectPossibleCategories(v, w, d1);
    		//System.out.println(pc);
    		
    		if(firstClassSelection.isSelected() == true) {
    			if(pc.contains("1")) {
    				fc = new FirstClass(v, w, d1 , i, p, from, to);
    				if(i.fragile == true) {
    					fc.setBroken(true);
    				}	
    			} else {
    				errorLabel.setText("Pakettia ei voida lähettää tässä luokassa\n"
    						+ "Tarkista: tilavuus "+ ((double)v)/1000000+"m3, etäisyys "+d1+"km");
    				return;
    			}
    		} else if(scndClassSelection.isSelected() == true) {
    			if(pc.contains("2")) {
    				fc = new SecondClass(v, w, d1 , i, p, from, to);
    			} else {
    				errorLabel.setText("Pakettia ei voida lähettää tässä luokassa\n"
    						+ "Tarkista: tilavuus "+ ((double)v)/1000000+"m3, etäisyys "+d1+"km");
    				return;
    			}
    		} else if(thrdClassSelection.isSelected() == true) {
    			if(pc.contains("3")) {
    				fc = new ThirdClass(v, w, d1 , i, p, from, to);
    				if(i.fragile == true) {
    					fc.setBroken(true);
    				}
    				
    			} else {
    				errorLabel.setText("Pakettia ei voida lähettää tässä luokassa\n"
    						+ "Tarkista: tilavuus "+ ((double)v)/1000000+"m3, etäisyys "+d1+"km");
    				return;
    			}
    		}
    		
    		
    		
    	
    		Storage.getInstance().storageList.add(fc);
    			
    		packageCombo.getItems().add(fc);
    		
    		
    		cancelNewPackageAction(event);
    		firstClassInfoAction(event);
    		
    		
    }
    
    @FXML
    void cancelNewPackageAction(ActionEvent event) {
    		
    		mainTabPane.getSelectionModel().select(0);
    		
    		itemComboBox.getSelectionModel().clearSelection();
    		toCityComboBox.getSelectionModel().clearSelection();
    		fromCityComboBox.getSelectionModel().clearSelection();
    		toSmartComboBox.getSelectionModel().clearSelection();
    		fromSmartComboBox.getSelectionModel().clearSelection();
    		widthField.setText("");	
    		heightField.setText("");
    		depthField.setText("");
    		itemNameField.setText("");
    		itemWeightField.setText("");
    		fragileCheckBox.setSelected(false);
    		errorLabel.setText("");
    		firstClassSelection.setSelected(true);
    		
    		
    		
    		

    }


    @FXML
    void addSmartPostToSmartpostCombo(ActionEvent event) {
    	ArrayList<Smartpost> spByCity_array;
    smartpostHandler.getInstance();
    spByCity_array=smartpostHandler.getInstance().getSmartpostListByCity(toCityComboBox.valueProperty().getValue());
    ObservableList<Smartpost> oList = FXCollections.observableArrayList(spByCity_array);
	toSmartComboBox.setItems(oList);
    }

    @FXML
    void addSmartpostsFromSmartpostsCombo(ActionEvent event) {
    	ArrayList<Smartpost> spByCity_array;
        smartpostHandler.getInstance();
        spByCity_array=smartpostHandler.getInstance().getSmartpostListByCity(fromCityComboBox.valueProperty().getValue());
        ObservableList<Smartpost> oList = FXCollections.observableArrayList(spByCity_array);
    		fromSmartComboBox.setItems(oList);
    }
    
    
    
    @FXML
    void firstClassInfoAction(ActionEvent event) {
    		classInfoLabel.setText("Tilavuus max 0,216m3 (60x60x60cm)\n"
    					+ "Paino max 20kg\n"
    					+ "Paketin voi lähettää enintään 150km päähän\n"
    					+ "Särkyvät paketit todennäköisesti rikkoutuvat");
    		
    }

    @FXML
    void scndClassInfoAction(ActionEvent event) {
    		classInfoLabel.setText("Tilavuus max 0,064m3 (40x40x40cm)\n"
					+ "Paino max 20kg\n"
					+ "Paketin voi lähettä mihin tahansa Suomen pakettiautomaattiin\n"
					+ "Erittäin turvallinen särkyville paketeille");

    	}


    @FXML
    void thrdClassInfoAction(ActionEvent event) {
    		classInfoLabel.setText("Ei tilavuus, paino tai etäisyysrajoitteita\n"
    				+ "Särkyvien pakettien lähettämistä ei suositella");
    }

    @FXML
    void showHistory(ActionEvent event) {
    	try {
	    	BufferedReader in = new BufferedReader(new FileReader("Loki.txt"));
			String line;
			
			packageView.getItems().clear();
			
		    while((line = in.readLine()) != null) {
		    	packageView.getItems().add(line);
		    }
			in.close();
			sumOfPackages.setText(Integer.toString(packageView.getItems().size()));
    	} catch (IOException ex) {
    		System.err.println("Tiedoston luku ei onnistunut"+ex.getMessage());
    	}
    }
    
    @FXML
    void getLogByDateAction(ActionEvent event) {
    	
   
    	
       	
    	try {
    		DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    	
    		String date = logDatePicker.getValue().format(dateFormatter);
    		
	    	BufferedReader in = new BufferedReader(new FileReader("Loki.txt"));
			String line;
			
			packageView.getItems().clear();
			
		    while((line = in.readLine()) != null) {
		    		if(line.contains(date)) {
		    			packageView.getItems().add(line);
		    		}

		    }
			in.close();		
			sumOfPackages.setText(Integer.toString(packageView.getItems().size()));
    	
    	} catch (IOException ex) {
    		System.err.println("Tiedoston luku ei onnistunut"+ex.getMessage());
    	} catch (NullPointerException ex) {
    		
    	}

    }
}
