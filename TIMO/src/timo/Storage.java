package timo;

import java.util.ArrayList;

public class Storage {
	
	public ArrayList<Package> storageList;
	
	private static Storage strg = null;
	
	//Singleton
	public static Storage getInstance() {
		if(strg == null) {
			strg = new Storage();
		}
		return strg;
	}

	
	public Storage() {
		
		storageList = new ArrayList<Package>();
		
	}
	
	public String selectPossibleCategories(int v, double w, int d) {
		if(d > 150 || w > 20) {
			if(w > 20 || v > 0.064*1000000) {
				return "3";
			} else {
				return "23";
			}
			
		} else if(v > 0.216*1000000) { 
			return "3";
	
		} else if(v >= 0.064*1000000 && v <= 0.216*1000000) {
			if(w > 20 || d > 150) {
				return "3";
			} else {
				return "13";
			}
			
		} else {
			return "123";
		}
		
		
	}


	public ArrayList<Package> getStorageList() {
		return storageList;
	}
	
}
