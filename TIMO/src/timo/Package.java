package timo;

import java.util.ArrayList;

public abstract class Package {
	
	protected int volume; //measuring the size of the package as volume
	protected double weight;
	protected int traveldist; 
	protected int category;
	protected boolean isBroken;
	protected Item item;
	protected ArrayList<String> fromToPoints;
	protected Smartpost fromSmartpost;
	protected Smartpost toSmartpost;
	
	
	
	public Package(int v, double w, int d, int c, Item it, ArrayList<String> ftp, Smartpost from, Smartpost to) {
		
		volume = v;
		weight = w;
		traveldist = d;
		category = c;
		item = it;
		fromToPoints = ftp;
		fromSmartpost = from;
		toSmartpost = to;
		isBroken = false;
		
	}
	
	public boolean isBroken() {
		return isBroken;
	}

	public void setBroken(boolean isBroken) {
		this.isBroken = isBroken;
	}

	@Override
	public String toString() {
		return item.getName()+", "+category+". Luokka";
		
	}
	
	


}

