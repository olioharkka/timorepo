package timo;

public class Item {
	
	int volume;
	double weight;
	String name;
	boolean fragile;
	boolean isBroken;

	
	public Item(int v, double w, String n, boolean f) {
		
		volume = v;
		weight = w;
		name = n;
		fragile = f;
		//System.out.println("Esine on luotu");
		
	}
	


	@Override
	public String toString() {
		return name;
	}

	
	
	public int getVolume() {
		return volume;
	}

	public double getWeight() {
		return weight;
	}

	public String getName() {
		return name;
	}

}
