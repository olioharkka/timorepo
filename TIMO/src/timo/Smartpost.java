package timo;

public class Smartpost {
	private String code;
	private String city;
	private String address;
	private String availability;
	private String postoffice;
	private String lat;
	private String lng;
	
	
    public Smartpost(String co, String ci, String ad, String av, String p, String la, String ln) {
    		code = co;
    		city = ci;
        address = ad;
        availability = av;
        postoffice = p;
        lat = la;
        lng = ln;
    }
    
    
    @Override
    public String toString() {
    	
    	return (city + " " + postoffice); //this is for the comboboxes
    }

    public String getCode() {
		return code;
	}
    
	public String getCity() {
		return city;
	}

	public String getAddress() {
		return address;
	}

	public String getPostoffice() {
		return postoffice;
	}


	public String getAvailability() {
		return availability;
	}


	public String getLat() {
		return lat;
	}


	public String getLng() {
		return lng;
	}
    
	
    
}
