package timo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;



public class smartpostHandler {
	
	private ArrayList<String> city_array;
	private ArrayList<Smartpost> smartpost_array;
	private ArrayList<Smartpost> spByCity_array;
    private Document doc;
    static private smartpostHandler sph = null;
	
	public smartpostHandler(String u) {
		String line;
		String content="";
        
        try {
        		URL url = new URL(u); //URL content to String
        		BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        	
        		
				while ((line = br.readLine()) != null) {
        			content += line + "\n";
        		}
        	
        		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			
			doc = dBuilder.parse(new InputSource(new StringReader(content))); //Making document out of content
			doc.getDocumentElement().normalize();
			
			parseSmartpostData();
			
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	static public smartpostHandler getInstance() {
		if (sph == null)
			sph = new smartpostHandler("http://smartpost.ee/fi_apt.xml");
		return sph;
	}
	
	public void parseSmartpostData() {
		smartpost_array = new ArrayList<Smartpost>();
		city_array = new ArrayList<String>();
		int k = 0;
		int l = 0;
		String code, city, address, availability, postoffice, lat, lng;
		String previouscity=" ";
		
		NodeList nodes = doc.getElementsByTagName("place"); //Data to every Smartpost machine
		
		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if(node.getNodeType()==Node.ELEMENT_NODE) {
				Element e = (Element) node;
				
				//Every single detail about Smartpost machine
				code=e.getElementsByTagName("code").item(0).getTextContent();
				city=e.getElementsByTagName("city").item(0).getTextContent();				
				address = e.getElementsByTagName("address").item(0).getTextContent();
				availability =e.getElementsByTagName("availability").item(0).getTextContent();
				postoffice = e.getElementsByTagName("postoffice").item(0).getTextContent();
				lat = e.getElementsByTagName("lat").item(0).getTextContent();
				lng = e.getElementsByTagName("lng").item(0).getTextContent();
				
				//Creating ArrayList which contains Smartpost objects
				smartpost_array.add(k, new Smartpost(code, city, address, availability, postoffice, lat, lng));
				
				//Creating city array for comboboxes
				if (city_array.isEmpty()) {
					city_array.add(l, city);
					l++;
				}
				else if (city_array.get(l-1).equals(previouscity)) {

				}
				else {
					city_array.add(l, city);
					l++;
				}

				previouscity=city;
				k=k+1;
				
			}
		}
	}
	
	public ArrayList<Smartpost> getSmartpostListByCity(String city) {
		spByCity_array = new ArrayList<Smartpost>();
		
		for(int i=0; i<getSmartpost_array().size(); i++) {
    		if(getSmartpost_array().get(i).getCity().equals(city)) {
    			spByCity_array.add(getSmartpost_array().get(i));
    		}
    		}
		return spByCity_array;
	}

	public ArrayList<Smartpost> getSmartpost_array() {
		return smartpost_array;
	}

	public ArrayList<String> getCity_array() {
		return city_array;
	}

	
}
